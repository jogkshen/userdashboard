﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public static class LoginData
    {

        public static string conection = "Data Source=Acer//SQLSERVER;Initial Catalog=projectuserdashboard;Integrated Security=True";
        public static SqlConnection Connection() {
            SqlConnection connection = new SqlConnection(conection);
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }return connection;
        }

        public static DataTable Login_Data(String query, SqlParameter[] parameters) {
            using (SqlConnection connection = Connection()) {
                SqlCommand command = new SqlCommand(query, connection);
                if (parameters != null) {
                    command.Parameters.AddRange(parameters);
                }
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable datatable = new DataTable();
                adapter.Fill(datatable);
                return datatable;

            }
        }

    }
}
