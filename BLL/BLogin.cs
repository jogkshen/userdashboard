﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;


namespace BLL
{
    public class BLogin
    {
        public DataTable getDataFromUser(string UserName, string Password)
        {
            SqlParameter[] par = new SqlParameter[]
            {
                new SqlParameter("@UserName",UserName),
                new SqlParameter("@Password",Password)
            };
            return connection.getuser("select * from tbl_userregister where Username=@UserName and Password=@Password", par);
        }
        public int inputdatauser(string Fullname, string UserName, string Password, string Subject, string email)
        {
            SqlParameter[] par = new SqlParameter[]
            {
                new SqlParameter("@fullname",Fullname),
                new SqlParameter("@UserName",UserName),
                 new SqlParameter("@Password",Password),
                  new SqlParameter("@Subject",Subject),
                  new SqlParameter("@email",email),
            };
            return connection.IDU ("insert into tbl_userregister values (@fullname,@UserName,@Password,@subject,@email)", par);

        }

        public int updateuser(string Fullname, String UserName, string Password, string Subject, string email, int UserID)
        {
            SqlParameter[] par = new SqlParameter[]
            {

                      new SqlParameter("@fullname",Fullname),
                new SqlParameter("@UserName",UserName),
                 new SqlParameter("@Password",Password),
                  new SqlParameter("@Subject",Subject),
                  new SqlParameter("@email",email),
                  new SqlParameter("@UserID",UserID)
            };
            
            return connection.IUD_SP("update tbl_userregister set Fullname=@fullname, Username=@UserName, Password=@Password, subject=@Subject, email=@email where UserID=@UserID", CommandType.Text, par);

        }
        public int updatedata(string Title, Byte[] Datafile, int DataID)
        {
            SqlParameter[] par = new SqlParameter[]
            {
                 new SqlParameter("@Title",Title),
               new SqlParameter("@Datafile",Datafile),
                new SqlParameter("@DataID",DataID)
            };
            return connection.IUD_SP("update tbl_data set Title=@Title, Datafile=@Datafile where DataID=@DataID", CommandType.Text, par);


        }


        public DataTable getuser()
        {
            DataTable dt = new DataTable();
            dt = connection.getuser("select Fullname,Username,Subject,email from tbl_userregister", null);
            return dt;
        }

        public DataTable showdata()
        {
            DataTable dt = new DataTable();
            dt = connection.getuser ("select Title from tbl_data", null);
            return dt;
        }
        public int DeleteUser(int UserID)
        {
            SqlParameter[] par = new SqlParameter[]
            {

               new SqlParameter("@UserID",UserID)

            };
            int i = 0;
            i = connection.IUD_SP("delete from tbl_userregister where UserID=@UserID", CommandType.Text, par);
            return i;
        }
        public int ChangePassword(string username, string newpassword)
        {
            SqlParameter[] par = new SqlParameter[]
            {
           new SqlParameter("@username",username),
            new SqlParameter("@password",newpassword),


            };
            int i = 0;
            i = connection.IUD_SP("Update tbl_userregister set Password=@password where Username=@username", CommandType.Text, par);
            return i;
        }
        public int addfiles(string Title,byte[] Datafile)
        {
            SqlParameter[] par = new SqlParameter[]
            {

               new SqlParameter("@Title",Title),
               new SqlParameter("@Datafile",Datafile)
              
            };
            return connection.IUD_SP("insert into tbl_data(Title,Datafile) values (@Title,@Datafile)", CommandType.Text, par);
        }
        public byte[] filereader(string sPath)
        {
            //Initialize byte array with a null value initially.
            byte[] data = null;

            //Use FileInfo object to get file size.
            FileInfo fInfo = new FileInfo(sPath);
            long numBytes = fInfo.Length;

            //Open FileStream to read file
            FileStream fStream = new FileStream(sPath, FileMode.Open, FileAccess.Read);

            //Use BinaryReader to read file stream into byte array.
            BinaryReader br = new BinaryReader(fStream);

            //When you use BinaryReader, you need to supply number of bytes to read from file.
            //In this case we want to read entire file. So supplying total number of bytes.
            data = br.ReadBytes((int)numBytes);
            return data;
        }

        public int Deletedata(int DataID)
        {
            SqlParameter[] par = new SqlParameter[]
            {

               new SqlParameter("@DataID",DataID)

            };
            int i = 0;
            i = connection.IUD_SP("delete from tbl_data where DataID=@DataID", CommandType.Text, par);
            return i;
        }
        public int Managedata(int DataID)
        {
            SqlParameter[] par = new SqlParameter[]
            {

               new SqlParameter("@DataID",DataID)

            };
            int i = 0;
            i = connection.IUD_SP("delete from tbl_data where DataID=@DataID", CommandType.Text, par);
            return i;
        }



    }
}
