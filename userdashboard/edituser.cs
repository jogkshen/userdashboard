﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userdashboard
{
    public partial class edituser : Form
    {
        public edituser()
        {
            InitializeComponent();
        }
        public int UserID = 0;
        BLogin bl = new BLogin();
        private void button1_Click(object sender, EventArgs e)
        {
            int i = bl.updateuser(txtfullname.Text,txtusername.Text, txtpassword.Text, txtsubject.Text, txtemail.Text, UserID);
            if (i > 0)
            {
                MessageBox.Show("User Updated");
                dashboard db = new dashboard();
                db.Show();
                this.Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int i = bl.DeleteUser(UserID);
            if (i > 0)
            {
                MessageBox.Show("User Deleted");
                dashboard db = new dashboard();
                db.Show();
                this.Hide();
            }
        }

        private void edituser_Load(object sender, EventArgs e)
        {

        }
    }
}
