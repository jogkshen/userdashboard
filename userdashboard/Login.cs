﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userdashboard
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        BLogin blg = new BLogin();
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtusername.Text==""||txtusername.Text==null)
            {
                MessageBox.Show("please fill the username", "Empty field", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtusername.Focus();

            }
            else if (txtpassword.Text==""||txtpassword.Text== null)
            {
                MessageBox.Show("please fill password", "Empty textbox", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                DataTable dt = blg.getDataFromUser(txtusername.Text, txtpassword.Text);
                if (dt.Rows.Count > 0)
                {
                    if (txtusername.Text.Equals("admin"))
                    {
                        MessageBox.Show("login success");
                        dashboard ds = new dashboard();
                        ds.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("login success");
                        teacherdashboard td = new teacherdashboard();
                        td.Visible = true;
                    }
                }
                else
                {
                    MessageBox.Show("Invalid username and password", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            this.Hide();
        }
    }
}
