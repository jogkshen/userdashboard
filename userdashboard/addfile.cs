﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;


namespace userdashboard
{
    public partial class addfile : UserControl
    {
        public addfile()
        {
            InitializeComponent();
        }
        public int DataID = 0;
        BLogin blg = new BLogin();


        private void button1_Click(object sender, EventArgs e)
        {
            byte[] dfile = blg.filereader(txtfile.Text);

            int i = blg.addfiles(txttitle.Text, dfile);
            if (i > 0)
            {
                MessageBox.Show("data added");
                teacherdashboard td = new teacherdashboard(); 
                td.Show();

            }
   

        }

        private void button3_Click(object sender, EventArgs e)
        {

            openFileDialog1.Filter = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtfile.Text = openFileDialog1.FileName;
                
            }
        }

        private void addfile_Load(object sender, EventArgs e)
        {

        }

        private void txtfile_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
