﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace userdashboard
{
    public partial class teacherdashboard : Form
    {
        public teacherdashboard()
        {
            InitializeComponent();
        }
        public int fileid = 0;
        BLogin bl = new BLogin();
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void addDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addfile af = new addfile();
            dataGridView1.Controls.Add(af);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void teacherdashboard_Load(object sender, EventArgs e)
        {
            DataTable dt = bl.showdata();
            if (dt.Rows.Count > 0)
            {
                dataGridView1.DataSource = dt;
            }
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            managedata md = new managedata();
            md.fileid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            md.txttitle.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            
            md.Show();
            this.Hide();
            
        }
    }
}
