﻿namespace userdashboard
{
    partial class edit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textfullname = new System.Windows.Forms.TextBox();
            this.textusername = new System.Windows.Forms.TextBox();
            this.textpassword = new System.Windows.Forms.TextBox();
            this.textsubject = new System.Windows.Forms.TextBox();
            this.textemail = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textfullname
            // 
            this.textfullname.Location = new System.Drawing.Point(23, 31);
            this.textfullname.Name = "textfullname";
            this.textfullname.Size = new System.Drawing.Size(151, 20);
            this.textfullname.TabIndex = 0;
            // 
            // textusername
            // 
            this.textusername.Location = new System.Drawing.Point(23, 58);
            this.textusername.Name = "textusername";
            this.textusername.Size = new System.Drawing.Size(151, 20);
            this.textusername.TabIndex = 1;
            // 
            // textpassword
            // 
            this.textpassword.Location = new System.Drawing.Point(23, 85);
            this.textpassword.Name = "textpassword";
            this.textpassword.Size = new System.Drawing.Size(151, 20);
            this.textpassword.TabIndex = 2;
            // 
            // textsubject
            // 
            this.textsubject.Location = new System.Drawing.Point(23, 112);
            this.textsubject.Name = "textsubject";
            this.textsubject.Size = new System.Drawing.Size(151, 20);
            this.textsubject.TabIndex = 3;
            // 
            // textemail
            // 
            this.textemail.Location = new System.Drawing.Point(23, 139);
            this.textemail.Name = "textemail";
            this.textemail.Size = new System.Drawing.Size(151, 20);
            this.textemail.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 177);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(51, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(60, 177);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "delete";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(136, 177);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(69, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textemail);
            this.Controls.Add(this.textsubject);
            this.Controls.Add(this.textpassword);
            this.Controls.Add(this.textusername);
            this.Controls.Add(this.textfullname);
            this.Name = "edit";
            this.Size = new System.Drawing.Size(214, 211);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textfullname;
        private System.Windows.Forms.TextBox textusername;
        private System.Windows.Forms.TextBox textpassword;
        private System.Windows.Forms.TextBox textsubject;
        private System.Windows.Forms.TextBox textemail;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}
