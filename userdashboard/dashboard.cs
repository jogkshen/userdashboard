﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BLL;


namespace userdashboard
{
    public partial class dashboard : Form
    {
        public dashboard()
        {
            InitializeComponent();
        }

        BLogin bl = new BLogin();
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            adduser au = new adduser();
            pnldashboard.Controls.Add(au);
        }

        private void pnldashboard_Paint(object sender, PaintEventArgs e)
        {
        }

     

        private void dashboard_Load(object sender, EventArgs e)
        {
            DataTable dt = bl.getuser();
            if(dt.Rows.Count>0)
            {
                dataGridView1.DataSource = dt;
            }
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            edituser frm = new edituser();
            frm.UserID = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            frm.txtfullname.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            frm.txtusername.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            frm.txtpassword.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            frm.txtsubject.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            frm.txtemail.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            frm.Show();
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
    }

      

